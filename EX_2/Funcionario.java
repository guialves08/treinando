
public class Funcionario
{
    private String nome;
    private double salarioBase, totalVendas;
    private int numeroFilhos;
    private boolean valeTransporte, trabalhoNoite, ehVendedor;
    
    public double inss(){
        return  salarioBase * 0.13;
    }
    
    public double bonusFamilia(){
        if (numeroFilhos <= 3){
            return numeroFilhos * 50;
        }else{
            return 150;
        } 
    }
    
    public double descontoVale(){
        if (valeTransporte){
            return salarioBase * 0.03;
        }else{
            return 0;
        }
    }
    
    public double recebeAddNoturno(){
        if (trabalhoNoite){
            return salarioBase * 0.05;
        }else{
            return 0;
        }
    }
    
    public double percentualVendas(){
        if (ehVendedor){
            return totalVendas * 0.05;
        }else{
            return 0;
        }
    }
    
    
    
    
    //REFATURAÇÃO
    public double salarioLiquido(){
        double sal = salarioBase - inss() + bonusFamilia()
                - descontoVale() + recebeAddNoturno() + percentualVendas();
        
        return sal;
    }
    
    
    public Funcionario(){
        
    }
    public Funcionario(String nome){
        this.nome = nome;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getNome(){
        return nome;
    }
    
    public void setSalarioBase(double salarioBase){
        this.salarioBase = salarioBase;
    }
    public double getSalarioBase(){
        return salarioBase;
    }
    
    public void setTotalVendas(double totalVendas){
        this.totalVendas = totalVendas;
    }
    public double getTotalVendas(){
        return totalVendas;
    }
    
    public void setNumeroFilhos(int numeroFilhos){
        if (numeroFilhos >= 0){
            this.numeroFilhos = numeroFilhos;
        }
    }
    public int getNumeroFilhos(){
        return numeroFilhos;
    }
    
    public void setValeTransporte(boolean valeTransporte){
        this.valeTransporte = valeTransporte;
    }
    public boolean getValeTransporte(){
        return valeTransporte;
    }
    
    public void setTrabalhoNoite(boolean trabalhoNoite){
        this.trabalhoNoite = trabalhoNoite;
    }
    public boolean getTrabalhoNoite(){
        return trabalhoNoite;
    }
    
    public void setEhVendedor(boolean ehVendedor){
        this.ehVendedor = ehVendedor;
    }
    public boolean getEhVendedor(){
        return ehVendedor;
    }
    
}
